/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */


#include <unistd.h>
#include <stdlib.h>
#include <ck_task.h>

int main()
{
    main_thread();
    
    return 0;
}